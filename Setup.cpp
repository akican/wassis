
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Setup.h"
#include <system.hpp>
#include <IniFiles.hpp>
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSetupForm *SetupForm;
//---------------------------------------------------------------------------
__fastcall TSetupForm::TSetupForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::TabSheet2Show(TObject *Sender)
{
     DateList->Items->Add(FormatDateTime("yyyy/mm/dd aaaa hh:nn:ss",Now()));
     DateList->Items->Add(FormatDateTime("yy/mm/dd(aaa) AM/PM hh:mm:ss",Now()));
     DateList->ItemIndex = MainForm->date;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::S_CanselBtnClick(TObject *Sender)
{
     Close();
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::S_OKBtnClick(TObject *Sender)
{
     String path = ExtractFileDir(Application->ExeName);
     path = path + "\\setup.ini";
     TIniFile *setini = new TIniFile(path);
     int tm;
     tm = DateList->ItemIndex;
     if(tm == -1)
      setini->WriteInteger("DATE","Type",0);
     else
      setini->WriteInteger("DATE","Type",tm);
     setini->WriteBool("Startup","IME",S_StatupIMEchk->Checked);
     setini->WriteBool("Shutdown","Bookmark",S_ShutdownBookmark->Checked);
     setini->WriteBool("Save","CreateBackup",S_BackupCheck->Checked);
     setini->WriteString("Infomation","Sign",SignText->Lines->Text);
     delete setini;
     Close();
}
//---------------------------------------------------------------------------
