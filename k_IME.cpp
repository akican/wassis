//---------------------------------------------------------------------------
#pragma hdrstop

#include "k_IME.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
K_IME::K_IME(HWND hhandle,DWORD Keylayout)
{
        hwnd = SetFocus(hhandle);
        himc = ImmGetContext(hwnd);
        ime_opened = ImmGetOpenStatus(himc);
        ImmGetConversionStatus(himc,&dwConv,&dwSentence);
        hkl = GetKeyboardLayout(Keylayout);
}
//---------------------------------------------------------------------------
K_IME::K_IME()
{

}
//---------------------------------------------------------------------------
K_IME::~K_IME()
{
        if(himc != NULL)
         {
            ImmReleaseContext(hwnd,himc);
            himc = NULL;
         }

        if(hwnd != NULL)
          hwnd = NULL;

        if(hkl != NULL)
          hkl = NULL;
}
//---------------------------------------------------------------------------
void K_IME::Init(HWND hhandle,DWORD Keylayout)
{
        hwnd = SetFocus(hhandle);
        himc = ImmGetContext(hwnd);
        ime_opened = ImmGetOpenStatus(himc);
        ImmGetConversionStatus(himc,&dwConv,&dwSentence);
        hkl = GetKeyboardLayout(Keylayout);
}
//---------------------------------------------------------------------------
void K_IME::Switch(BOOL mode)
{
        ImmSetOpenStatus(himc,mode);
}
//---------------------------------------------------------------------------
BOOL K_IME::Opened()
{
        return ime_opened;
}
//---------------------------------------------------------------------------
int K_IME::set_InputType(int mode)
{
        switch(mode)
          {
                case KIME_ROMA:
                  ImmSetConversionStatus(himc,dwConv | IME_CMODE_ROMAN,dwSentence);
                 break;

                case KIME_KANA:
                  ImmSetConversionStatus(himc,dwConv ^ IME_CMODE_ROMAN,dwSentence);
                 break;

          }
        return mode;
}
//---------------------------------------------------------------------------
int K_IME::get_InputType()
{
        int mode = -1;

        if((dwConv & IME_CMODE_ROMAN) == IME_CMODE_ROMAN)
          mode = KIME_ROMA;
        else
          mode = KIME_KANA;

        return mode;
}
//---------------------------------------------------------------------------
int K_IME::set_CharType(int mode)
{
        DWORD inputtype;
        if(get_InputType() == KIME_ROMA)
          inputtype = IME_CMODE_ROMAN;
        else if(get_InputType() == KIME_KANA)
          inputtype = 0;

        switch(mode)
          {
              case KIME_HIRAGANA:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_NATIVE | IME_CMODE_FULLSHAPE,dwSentence);
               break;

              case KIME_WIDEKATAKANA:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_NATIVE | IME_CMODE_FULLSHAPE | IME_CMODE_KATAKANA,dwSentence);
               break;

              case KIME_KATAKANA:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_NATIVE | IME_CMODE_KATAKANA,dwSentence);
               break;

              case KIME_WIDEALPHABET:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_FULLSHAPE,dwSentence);
               break;

              case KIME_ALPHABET:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_ALPHANUMERIC,dwSentence);
               break;

              case KIME_DIRECT:
                ImmSetConversionStatus(himc,inputtype | IME_CMODE_NATIVE,dwSentence);
               break;

         }
        return mode;
}
//---------------------------------------------------------------------------
int K_IME::get_CharType()
{
        int mode = -1;
/*
        DWORD inputtype;
        if(get_InputType() == KIME_ROMA)
          inputtype = IME_CMODE_ROMAN;
        else if(get_InputType() == KIME_KANA)
          inputtype = 0;
*/
       switch(dwConv ^ IME_CMODE_ROMAN)
          {
              case IME_CMODE_NATIVE | IME_CMODE_FULLSHAPE:
                mode = KIME_HIRAGANA;
               break;

              case IME_CMODE_NATIVE | IME_CMODE_FULLSHAPE | IME_CMODE_KATAKANA:
                mode = KIME_WIDEKATAKANA;
               break;

              case IME_CMODE_NATIVE | IME_CMODE_KATAKANA:
                mode = KIME_KATAKANA;
               break;

              case IME_CMODE_FULLSHAPE:
                mode = KIME_WIDEALPHABET;
               break;

              case IME_CMODE_ALPHANUMERIC:
                mode = KIME_ALPHABET;
               break;

              case IME_CMODE_NATIVE:
                mode = KIME_DIRECT;
               break;
          } 
           
       return mode;
}
//---------------------------------------------------------------------------
void K_IME::open_Dialog(DWORD config)
{
         ImmConfigureIME(hkl,hwnd,config,0);
}
//---------------------------------------------------------------------------]
void K_IME::get_String(char *string,int length)
{
         long ret;
         ret = ImmGetCompositionString(himc,KIME_STRING,string,length);
         string[ret] = '\0';
}


