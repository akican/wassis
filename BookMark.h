//---------------------------------------------------------------------------
#ifndef BookMarkH
#define BookMarkH
#include <IniFiles.hpp>
//---------------------------------------------------------------------------
class TBookMark
{
      private:

         TMenuItem *Top;
         int chk;
         String LastMark;
         TMenuItem *BookMarkMenu;
         int MaxMarkItems;
         String ReadFileName;
         String BookMarkName;
         void RebuildMark();
         void __fastcall BookMarkItemClick(TObject *Sender);
         void __fastcall ReadBookMarkList();
      public:
         TStringList *BookMarkList;
         TBookMark(int theMaxBookMarkItems,TMenuItem *theBookMarkMenu,TMenuItem* theTop);
         ~TBookMark(void);
        //void MaintainTBookMark(String Line);
};
//--------------------------------------------------------------------------
#endif
 