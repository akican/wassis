//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ClipbordHitory.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClipboardForm *ClipboardForm;
//---------------------------------------------------------------------------
__fastcall TClipboardForm::TClipboardForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TClipboardForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
      MainForm->clipformchk = false;
      Action = caFree;
}
//---------------------------------------------------------------------------
void __fastcall TClipboardForm::FormDeactivate(TObject *Sender)
{
     for(int pos = 0;pos < MainForm->cliphist->Count;pos ++)
      {
         if(SendMessage(ClipboardHistoryList->Handle,LB_FINDSTRING,-1,(LPARAM)MainForm->cliphist->Strings[pos].c_str()) == LB_ERR)
           ClipboardHistoryList->Items->Add(MainForm->cliphist->Strings[pos]);
      }
}
//---------------------------------------------------------------------------
void __fastcall TClipboardForm::ClipboardHistoryListDblClick(
      TObject *Sender)
{
      MainForm->Text->SelText = ClipboardHistoryList->Items->Strings[ClipboardHistoryList->ItemIndex];
}
//---------------------------------------------------------------------------

