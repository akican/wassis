//---------------------------------------------------------------------------
#ifndef SetupH
#define SetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TSetupForm : public TForm
{
__published:	// IDE 管理のコンポーネント
        TBitBtn *S_OKBtn;
        TBitBtn *S_CanselBtn;
        TBitBtn *S_RefreshBtn;
        TBevel *Bevel1;
        TPageControl *PageControl1;
        TTabSheet *TabSheet2;
        TLabel *Label1;
        TListBox *DateList;
        TCheckBox *S_StatupIMEchk;
        TBitBtn *BitBtn1;
        TMemo *SignText;
        TLabel *Label2;
        TCheckBox *S_ShutdownBookmark;
        TCheckBox *S_BackupCheck;
        void __fastcall TabSheet2Show(TObject *Sender);
        void __fastcall S_CanselBtnClick(TObject *Sender);
        void __fastcall S_OKBtnClick(TObject *Sender);
private:	// ユーザー宣言
public:		// ユーザー宣言
        __fastcall TSetupForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSetupForm *SetupForm;
//---------------------------------------------------------------------------
#endif
