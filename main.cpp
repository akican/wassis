//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "main.h"
#include "Setup.h"
#include <vcl\clipbrd.hpp>
#include <system.hpp>
#include <IniFiles.hpp>
#include <windows.hpp>
#include <windowsx.h>
#include "splashfo.h"
#include "linejump.h"
#include <FileCtrl.hpp>
#include <IniFiles.hpp>
#include <shellapi.h>
#include "ClipbordHitory.h"
#include "k_IME.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_NewClick(TObject *Sender)
{
    FileHistory->MaintainHistory("");
    if(Text->Modified == true)
      {
         switch(Application->MessageBox("ﾌｧｲﾙが変更されています。\n変更を保存しますか？","Coution!!",MB_YESNOCANCEL))
           {
              case IDYES:
               F_SaveClick(Sender);
               MainForm->filename = "";
               MainForm->backupfilename = "";
               Text->Lines->Clear();
               //MainForm->Caption  = "無題";
               Text->Modified = false;
               break;
             case IDNO:
               if(filename != "")
                CloseText();
               MainForm->filename = "";
               MainForm->backupfilename = "";
               Text->Lines->Clear();
               //MainForm->Caption  = "無題";
               Text->Modified = false;
           }

      }
    else
      {
         if(filename != "")
            CloseText();
         MainForm->filename = "";
         MainForm->backupfilename = "";
         Text->Lines->Clear();
         //MainForm->Caption  = "無題";
         Text->Modified = false;
      }
    Text->PlainText = false;
    bookmark = new TBookMark(9,BookMarkMenu,N20);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_OpenClick(TObject *Sender)
{
     if(OpenDialog->Execute())
      {
         OpenFile(OpenDialog->FileName,Sender);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_SaveClick(TObject *Sender)
{
     if(MainForm->filename == "" )
      {
         F_SaveAsClick(Sender);
      }
     else
      {
         if(createbackupchk)
          MainForm->backupfilename =  MainForm->CreateBackupFile(MainForm->filename);
         Text->Lines->SaveToFile(MainForm->filename);
         Text->Modified = false;
         CloseText();
      }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SeupMenuClick(TObject *Sender)
{
     TRect Rect = Application->MainForm->BoundsRect;
     TSetupForm *setupform = new TSetupForm(Application);
     setupform->S_StatupIMEchk->Checked = imechk;
     setupform->S_ShutdownBookmark->Checked =  MainForm->endbookmark;
     setupform->S_BackupCheck->Checked = MainForm->createbackupchk;
     setupform->SignText->Lines->Text = MainForm->Sign;
     setupform->Left = ((Rect.Right - Rect.Left) - setupform->Width)/2 + Rect.Left;
     setupform->Top = ((Rect.Bottom - Rect.Top) - setupform->Height)/2 + Rect.Top;
     setupform->ShowModal();
     delete setupform;

     ReadSetupini();

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_PrintClick(TObject *Sender)
{
     if(PrintDialog1->Execute())
     Text->Print("");
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_PageSetupClick(TObject *Sender)
{
     PrinterSetupDialog1->Execute();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_UndoClick(TObject *Sender)
{
     SendMessage(Text->Handle,EM_UNDO,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_CutClick(TObject *Sender)
{
     Text->CutToClipboard();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_CopyClick(TObject *Sender)
{
     Text->CopyToClipboard();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_PasteClick(TObject *Sender)
{
     Text->PasteFromClipboard();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_WordWorpClick(TObject *Sender)
{
     if(E_WordWorp->Checked == true)
      {
        Text->WordWrap = false;
        E_WordWorp->Checked = false;
      }
     else
      {
        Text->WordWrap = true;
        E_WordWorp->Checked = true;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::E_AllClick(TObject *Sender)
{
     Text->SelectAll();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_ReloadClick(TObject *Sender)
{
     String stmp = "編集中のFile 『"+ MainForm->filename;
     stmp = stmp + "』 を新しい名前で保存しますか？";
     switch(Application->MessageBox(stmp.c_str(),"Coution!!",MB_YESNO))
       {
           case IDYES:
             F_SaveAsClick(Sender);
             break;
       }
     Text->Lines->LoadFromFile(MainForm->filename);
     FileHistory->MaintainHistory(MainForm->filename);
     //MainForm->Caption  = MainForm->filename;
     Text->Modified = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::I_D_YearClick(TObject *Sender)
{
     switch(MainForm->date)
       {
          case 0:
            Text->SelText = FormatDateTime("yyyy",Now());
           break;

          case 1:
            Text->SelText = FormatDateTime("yy",Now());
           break;
       }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::S_SearchClick(TObject *Sender)
{
     if(FindDialog1->Execute());
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::S_ReplaceClick(TObject *Sender)
{
     ReplaceDialog1->Execute();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FindDialog1Find(TObject *Sender)
{
     TFindDialog *dialog = (TFindDialog *)(Sender);
     ftext = dialog->FindText;
     int position;
     String tmp;

     if(dialog->Options.Contains(frMatchCase))
       options << stMatchCase;

     if(dialog->Options.Contains(frWholeWord))
       options << stWholeWord;

     if(dialog->Options.Contains(frDown))
      {
        foption << frDown;
        int start = Text->SelStart;
        if(Text->SelLength != 0)
          ++ start;
        position = Text->FindText(ftext,start,Text->Text.Length() - Text->SelStart - 1,options);
      }
     else if(Text->SelStart > 0 )
      {
        foption >> frDown;
        int search = -1;
        do
         {
           position = search;
           search = Text->FindText(ftext,search + 1,Text->SelStart - search - 1,options);
         }
        while(search >= 0);
      }
     else
        position = -1;

     if(position >= 0)
      {
        Text->SelStart = position;
        Text->SelLength = ftext.Length();
        String value = ftext;
        if(SendMessage(Tool_Find->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
           Tool_Find->Items->Add(ftext);
        FindDialog1->CloseDialog();
      }
     else
      {
        tmp = "「" + ftext +"」が見つかりませんでした。";
        Application->MessageBox(tmp.c_str(),"検索",MB_OK);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ReplaceDialog1Replace(TObject *Sender)
{
     if(ReplaceDialog1->Options.Contains(frReplaceAll))
      {
        TSearchTypes options;
        if(ReplaceDialog1->Options.Contains(frMatchCase))
          options << stMatchCase;
        if(ReplaceDialog1->Options.Contains(frWholeWord))
          options << stWholeWord;

        int position = Text->FindText(ReplaceDialog1->FindText,0,-1,options);

        while(position >= 0)
          {
            Text->SelStart = position;
            Text->SelLength = ReplaceDialog1->FindText.Length();
            Text->SelText = ReplaceDialog1->ReplaceText;
            position = Text->FindText(ReplaceDialog1->FindText,position + 1,-position -2,options);
            ReplaceDialog1->CloseDialog();
          }
      }
     else if(Text->SelLength > 0)
      {
        Text->SelText = ReplaceDialog1->ReplaceText;
        FindDialog1Find(Sender);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_FontClick(TObject *Sender)
{
     if(Text->PlainText == true && Text->SelLength > 0)
      {
        switch(Application->MessageBox("現在のﾌｧｲﾙ形式では、選択した範囲のみのﾌｫﾝﾄ変更はできません。\nﾌｧｲﾙ形式をRTF(ﾘｯﾁﾃｷｽﾄ）形式に変更する必要があります。\n変更しますか？","注意",MB_YESNO))
           {
             case ID_YES:
               Text->PlainText = false;
               FontDialog1->Font->Assign(Text->SelAttributes);
               if(FontDialog1->Execute())
                 Text->SelAttributes->Assign(FontDialog1->Font);

             case ID_NO:
               FontDialog1->Font = Text->Font;
               if(FontDialog1->Execute())
                 Text->Font = FontDialog1->Font;
           }
      }
     else if( Text->PlainText == false)
      {
          Text->PlainText = false;
          FontDialog1->Font->Assign(Text->SelAttributes);
          if(FontDialog1->Execute())
             Text->SelAttributes->Assign(FontDialog1->Font);
      }
     else
      {
        FontDialog1->Font = Text->Font;
        if(FontDialog1->Execute())
           Text->Font = FontDialog1->Font;
      }

}
//---------------------------------------------------------------------------
void TMainForm::OpenFile(String file,TObject *Sender)
{
     Screen->Cursor = crHourGlass;
     if(filename != "")
          CloseText();
     if(Text->Modified == true)
      {
         switch(Application->MessageBox("ﾌｧｲﾙが変更されています。\n変更を保存しますか？","Caution!!",MB_YESNOCANCEL))
           {
             case IDYES:
               F_SaveClick(Sender);
               MainForm->filename = file;
               if(ExtractFileExt(file).UpperCase() == ".TXT")
                 Text->PlainText = true;
               else if(ExtractFileExt(file).UpperCase() == ".RTF" || ExtractFileExt(file).UpperCase() == ".DOC")
                 Text->PlainText = false;
               FileHistory->MaintainHistory(file);
               Text->Lines->LoadFromFile(MainForm->filename);
               OpenText();
               Text->Modified = false;
               break;
             case IDNO:
               MainForm->filename = file;
               if(ExtractFileExt(file) == ".txt")
                 Text->PlainText = true;
               else if(ExtractFileExt(file) == ".rtf")
                 Text->PlainText = false;
               FileHistory->MaintainHistory(file);
               Text->Lines->LoadFromFile(MainForm->filename);
               OpenText();
               Text->Modified = false;
           }
      }
     else
      {
         MainForm->filename = file;
         if(ExtractFileExt(file) == ".txt")
           Text->PlainText = true;
         else if(ExtractFileExt(file) == ".rtf")
           Text->PlainText = false;
         FileHistory->MaintainHistory(file);
         Text->Lines->LoadFromFile(MainForm->filename);
         OpenText();
         Text->Modified = false;
      }
     Screen->Cursor = crDefault;
}
//--------------------------------------------------------------------------
int CALLBACK EnumProc(LOGFONT *logfont,TEXTMETRIC *textmetric,DWORD type,LPARAM data)
{

    TComboBox *cb = (TComboBox *)data;
    cb->Items->Add(String(logfont->lfFaceName));

    return 1;
}
//--------------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender)
{
     Application->OnIdle = &OnIdle;
     DragAcceptFiles(Handle,true);
     EnumFontFamilies(Canvas->Handle,NULL,(FONTENUMPROC)EnumProc,LPARAM(FontBox));
     FontBox->ItemIndex = FontBox->Items->IndexOf(Text->Font->Name);
     if(SizeBox->ItemIndex == FontBox->Items->IndexOf(Text->Font->Size))
       SizeBox->ItemIndex = FontBox->Items->IndexOf(Text->Font->Size);
     UpdateText();
     FileHistory = new TFileHistory(9,"History.ini",F_History,u1,&OpenFile);
     MainForm->filename = "";
     foption << frDown;
     clipformchk = false;
     ftext = "";
     i =-1;
     Text->PlainText = false;
     cliphist = new TStringList;
     cliphist->Clear();
     ToolBar2->ButtonHeight = 24;
     ToolBar2->ButtonWidth = 25;
     Tool_LineJump->Height = 24;
     Tool_Find->Height = 22;
     FontBox->SelStart = 0;
     TabNo = 0;

     ReadSetupini();

}
//--------------------------------------------------------------------------
void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
     delete FileHistory;
     delete bookmark;
     delete cliphist;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FB_OthertextClick(TObject *Sender)
{
     String tmpfilepath;
     String bpath = ExtractFileDir(Application->ExeName);
     bpath = bpath + "\\backup\\";
     OpenBackDialog->InitialDir = bpath;
     if(OpenBackDialog->Execute())
      {
        OpenFile(OpenBackDialog->FileName,Sender);
      }
     OpenBackDialog->InitialDir = tmpfilepath;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_SaveAsClick(TObject *Sender)
{
     if(MainForm->filename == "")
      {
        MainForm->filename = "無題";
      }
     SaveDialog->FileName = MainForm->filename;

     if(Text->PlainText == false && Sender != F_SaveText)
      {
        SaveDialog->FileName = "*.rtf";
        SaveDialog->FilterIndex = 2;
      }
     else
      {
        SaveDialog->FileName = "*.txt";
        SaveDialog->FilterIndex = 1;
      }

     switch(SaveDialog->FilterIndex)
      {
        case 1:
           SaveDialog->DefaultExt = "txt";
          break;
        case 2:
           SaveDialog->DefaultExt = "rtf";
          break;
      }

     if(SaveDialog->Execute())
      {
         MainForm->filename = SaveDialog->FileName;
         if(Text->PlainText == false && SaveDialog->FilterIndex == 1)
          {
            switch(Application->MessageBox("現在のﾌｧｲﾙ形式では、書式情報は正しく保存されません。\nﾌｧｲﾙ形式をRTF(ﾘｯﾁﾃｷｽﾄ）形式に変更する必要があります。\n変更しますか？\n『いいえ』を選択すると書式情報を破棄したﾃｷｽﾄﾌｧｲﾙになります。","注意",MB_YESNOCANCEL))
              {
                 case ID_YES:
                   Text->PlainText = false;
                   CloseText();
                   if(createbackupchk)
                     MainForm->backupfilename =  MainForm->CreateBackupFile(MainForm->filename);
                   Text->Lines->SaveToFile(MainForm->filename);
                   FileHistory->MaintainHistory(MainForm->filename);
                   Text->Modified = false;
                   //MainForm->Caption  = MainForm->filename;
                  break;

                 case ID_NO:
                   Text->PlainText = true;


                   CloseText();
                   if(createbackupchk)
                     MainForm->backupfilename =  MainForm->CreateBackupFile(MainForm->filename);
                   Text->Lines->SaveToFile(MainForm->filename);
                   FileHistory->MaintainHistory(MainForm->filename);
                   Text->Modified = false;
                   //MainForm->Caption  = MainForm->filename;
                  break;
                 case ID_CANCEL:
                   MainForm->filename = "";
              }

          }
         else
          {

            CloseText();
            if(createbackupchk)
              MainForm->backupfilename =  MainForm->CreateBackupFile(MainForm->filename);
            Text->Lines->SaveToFile(MainForm->filename);
            FileHistory->MaintainHistory(MainForm->filename);
            Text->Modified = false;
            //MainForm->Caption  = MainForm->filename;
          }
      }
}
//---------------------------------------------------------------------------
void TMainForm::UpdateText()
{

     Text->Font->Charset = DEFAULT_CHARSET;
     Text->SelAttributes->Name = FontBox->Text;
     //Text->SelAttributes->Size = StrToInt(SizeBox->Text);

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_BulletClick(TObject *Sender)
{
/*     if(Text->PlainText == true)
      {
        switch(Application->MessageBox("現在のﾌｧｲﾙ形式では、位置情報は正しく保存されません。\nﾌｧｲﾙ形式をRTF(ﾘｯﾁﾃｷｽﾄ）形式に変更する必要があります。\n変更しますか？","注意",MB_OKCANCEL))
          {
            case MB_OK:
              Text->PlainText = false;
          }
      }
*/
     if(F_Bullet->Checked == false || T_BulletBtn->Down == true)
      {
        Text->Paragraph->Numbering = nsBullet;
        F_Bullet->Checked = true;
      }
     else
      {
        Text->Paragraph->Numbering = nsNone;
        F_Bullet->Checked = false;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_LeftClick(TObject *Sender)
{
/*     if(Text->PlainText == true)
      {
        switch(Application->MessageBox("現在のﾌｧｲﾙ形式では、位置情報は正しく保存されません。\nﾌｧｲﾙ形式をRTF(ﾘｯﾁﾃｷｽﾄ）形式に変更する必要があります。\n変更しますか？","注意",MB_OKCANCEL))
          {
            case MB_OK:
              Text->PlainText = false;
          }
      }
 */

     if(Sender == F_Left || Sender == T_LeftAlignBtn)
      {
        Text->WordWrap = true;
        E_WordWorp->Checked = true;
        F_Left->Checked = true;
        F_Center->Checked = false;
        F_Right->Checked = false;
        T_LeftAlignBtn->Down = true;
        T_CenterAlignBtn->Down = false;
        T_RightAlignBtn->Down = false;
        Text->Paragraph->Alignment = taLeftJustify;
      }
     else if(Sender == F_Center || Sender == T_CenterAlignBtn)
      {
        Text->WordWrap = true;
        E_WordWorp->Checked = true;
        F_Left->Checked = false;
        F_Center->Checked = true;
        F_Right->Checked = false;
        T_LeftAlignBtn->Down = false;
        T_CenterAlignBtn->Down = true;
        T_RightAlignBtn->Down = false;
        Text->Paragraph->Alignment = taCenter;
      }
     else if(Sender == F_Right || Sender == T_RightAlignBtn)
      {
        Text->WordWrap = true;
        E_WordWorp->Checked = true;
        F_Left->Checked = false;
        F_Center->Checked = false;
        F_Right->Checked = true;
        T_LeftAlignBtn->Down = false;
        T_CenterAlignBtn->Down = false;
        T_RightAlignBtn->Down = true;
        Text->Paragraph->Alignment = taRightJustify;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OnIdle(TObject *Sender,bool &Done)
{
     bool seltxt = Text->SelLength > 0;

     V_HistoryClipboard->Enabled = !clipformchk;

     if(Text->Paragraph->Numbering  == nsBullet)
       T_BulletBtn->Down = true;
     else
       T_BulletBtn->Down = false;

     if(filename == "")
      {
        B_Add->Enabled = false;
      }
     else
      {
        B_Add->Enabled = true;
      }
      
     if(Text->Modified == false || MainForm->filename == "")
      {
        F_Reload->Enabled = false;
      }
     else
      {
        F_Reload->Enabled = true;
      }

     if(MainForm->backupfilename == "")
      {
        FB_Thistext->Enabled = false;
      }
     else
      {
        FB_Thistext->Enabled = true;
      }
      
     E_Undo->Enabled = Text->Modified;
     if(Text->Modified == false)
      {
        StatusBar1->Panels->Items[1]->Text = "";
      }
     else
      {
        StatusBar1->Panels->Items[1]->Text = "変更あり";
      }

     if(MainForm->ftext == "")
      {
        S_Next->Enabled = false;
      }
     else
      {
        S_Next->Enabled = true;
      }

     StatusBar1->Panels->Items[2]->Text = MainForm->filename;

     E_Cut->Enabled = seltxt;
     E_Copy->Enabled = seltxt;
     if(seltxt == true && MainForm->Text->PlainText == true)
      {
        FontBox->Enabled = false;
        FontBox->Color = clInactiveCaptionText;
        SizeBox->Enabled = false;
        SizeBox->Color = clInactiveCaptionText;
      }
     else
      {
        FontBox->Enabled = true;
        FontBox->Color = clWindow;
        SizeBox->Enabled = true;
        SizeBox->Color = clWindow;
      }
     Clipboard()->Open();

     if(Clipboard()->AsText == "")
       E_Paste->Enabled = false;
     else
      {
        E_Paste->Enabled = true;
        int ind;
        if(!cliphist->Find(Clipboard()->AsText,ind))
          cliphist->Add(Clipboard()->AsText);
      }
     Clipboard()->Close();

     HIMC himc;
     Text->Focused();
     himc = ImmGetContext(GetFocus());
     J_Switch->Checked = ImmGetOpenStatus(himc);

     if(ImmGetOpenStatus(himc))
       StatusBar1->Panels->Items[3]->Text = "IME起動中";
     else
       StatusBar1->Panels->Items[3]->Text = "直接入力";

     DWORD dwConv,dwSentence,dwVal;
     HKL chkl;
     himc = ImmGetContext(GetFocus());
     ImmGetConversionStatus(himc,&dwConv,&dwSentence);
     dwVal = IME_CMODE_ROMAN;

     dwConv = dwConv & dwVal;
     if(dwConv == IME_CMODE_ROMAN)
       roma = true;
     else
       roma = false;

     if(roma)
      {
        IL_Kata->Checked = false;
        IL_Roma->Checked = true;
      }
     else
      {
        IL_Kata->Checked = true;
        IL_Roma->Checked = false;
      }

     chkl = GetKeyboardLayout(0L);
     ImmGetDescription(chkl,imename,100);
     ImmReleaseContext(Text->Handle,himc);


     T_RightAlignBtn->Enabled = !Text->PlainText;
     T_CenterAlignBtn->Enabled = !Text->PlainText;
     T_LeftAlignBtn->Enabled = !Text->PlainText;

     if(Text->PlainText == true)
      {
        F_Switch->Checked = false;
        F_Left->Enabled = false;
        F_Center->Enabled = false;
        F_Right->Enabled= false;
        F_Bullet->Enabled = false;
        E_WordWorp->Enabled = false;
        F_SaveText->Enabled = false;

      }
     else
      {
        F_Switch->Checked = true;
        F_Left->Enabled = true;
        F_Center->Enabled = true;
        F_Right->Enabled = true;
        F_Bullet->Enabled = true;
  /*      E_WordWorp->Enabled = true;
        FontBox->Enabled = true;
        FontBox->Color = clWindow;
        SizeBox->Enabled = true;
        SizeBox->Color = clWindow;*/
        F_SaveText->Enabled = true;

      }

     if(MainForm->Sign == "")
       I_Sign->Enabled = false;
     else
       I_Sign->Enabled = true;

     FontBox->Text = Text->SelAttributes->Name;
     SizeBox->Text = Text->SelAttributes->Size;

     UpdateStatusBar();
     Tool_JumpCont->Max = (SHORT)SendMessage(Text->Handle,EM_GETLINECOUNT,0,0);
     //Tool_JumpCont->Position = (int)SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0)+1;

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_BoldBtnClick(TObject *Sender)
{
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);

     if(T_BoldBtn->Down == true)
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin << fsBold;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
      }
     else
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin >> fsBold;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
        T_BoldBtn->Down = false;
      }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_ItaricBtnClick(TObject *Sender)
{
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);

     if(T_ItaricBtn->Down == true)
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin << fsItalic ;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
      }
     else
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin >> fsItalic;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
        T_ItaricBtn->Down = false;
      }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FontBoxChange(TObject *Sender)
{
     UpdateText();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::J_SwitchClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->Switch(!kime->Opened());
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::J_RegistWordClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->open_Dialog(KIME_WORDREGIST);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::J_ChangeDictionalyClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->open_Dialog(KIME_OPENDICTIONARY);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::J_PropatiyClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->open_Dialog(KIME_CONFIG);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Timer1Timer(TObject *Sender)
{
     switch(date)
       {
          case 0:
            StatusBar1->Panels->Items[4]->Text = FormatDateTime(" yyyy/mm/dd aaaa hh:nn:ss",Now());
           break;

          case 1:
            StatusBar1->Panels->Items[4]->Text = FormatDateTime(" yy/mm/dd(aaa) AM/PM hh:nn:ss",Now());
           break;
       }
     StatusBar1->Panels->Items[4]->Width = StatusBar1->Panels->Items[4]->Text.Length();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormResize(TObject *Sender)
{
     StatusBar1->Panels->Items[2]->Width = (StatusBar1->Width - (StatusBar1->Panels->Items[4]->Text.Length())) * 0.5;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TextKeyPress(TObject *Sender, char &Key)
{
     StatusBar1->Panels->Items[0]->Text = SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0) + 1;
     StatusBar1->Panels->Items[0]->Text = StatusBar1->Panels->Items[0]->Text + " 目";

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::I_D_AllClick(TObject *Sender)
{
     switch(date)
       {
          case 0:
            Text->SelText = FormatDateTime("yyyy/mm/dd aaaa hh:nn:ss",Now());
           break;

          case 1:
            Text->SelText = FormatDateTime("yy/mm/dd(aaa) AM/PM hh:nn:ss",Now());
           break;
       }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject *Sender)
{
     HIMC himc;
     Text->SetFocus();
     himc = ImmGetContext(GetFocus());
     ImmSetOpenStatus(himc,imechk);
     ImmReleaseContext(GetFocus(),himc);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::I_SignClick(TObject *Sender)
{
     Text->SelText = Sign;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::IL_RomaClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     if(kime->get_InputType() == KIME_KANA)
      {

        kime->set_InputType(KIME_ROMA);

      }
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::IL_KataClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     if(kime->get_InputType() == KIME_ROMA)
      {

        kime->set_InputType(KIME_KANA);

      }
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::IMH_KanaClick(TObject *Sender)
{
     HIMC himc;
     DWORD dwConv,dwSentence;
     Text->SetFocus();
     himc = ImmGetContext(GetFocus());
     ImmGetConversionStatus(himc,&dwConv,&dwSentence);
     ImmSetConversionStatus(himc,IME_CMODE_NATIVE | IME_CMODE_FULLSHAPE,dwSentence);
     ImmReleaseContext(Text->Handle,himc);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::JM_LhiraClick(TObject *Sender)
{

     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_HIRAGANA);
//     char tmp[KIME_ATOK12LIMIT];
//     kime->get_String(tmp);
    delete(kime);
//     MainForm->Caption = tmp;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::JW_LkataClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_WIDEKATAKANA);
     delete(kime);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::JM_SkanaClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_KATAKANA);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::JM_LalphaClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_WIDEALPHABET);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::JM_SalphaClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_KATAKANA);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::JW_AlphaClick(TObject *Sender)
{
     K_IME *kime = new K_IME(Text->Handle);
     kime->set_CharType(KIME_DIRECT);
     delete(kime);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_NewTextClick(TObject *Sender)
{
    FileHistory->MaintainHistory("");
    if(Text->Modified == true)
      {
         switch(Application->MessageBox("ﾌｧｲﾙが変更されています。\n変更を保存しますか？","Coution!!",MB_YESNOCANCEL))
           {
             case IDYES:
               F_SaveClick(Sender);
               MainForm->filename = "";
               Text->Lines->Clear();
               //MainForm->Caption  = "無題";
               Text->Modified = false;
               break;
             case IDNO:
               if(filename != "")
                 CloseText();
               MainForm->filename = "";
               Text->Lines->Clear();
               //MainForm->Caption  = "無題";
               Text->Modified = false;
           }

      }
    else
      {
         if(filename != "")
            CloseText();
         MainForm->filename = "";
         Text->Lines->Clear();
         //MainForm->Caption  = "無題";
         Text->Modified = false;
      }
    Text->PlainText = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_SwitchClick(TObject *Sender)
{
    if(Text->PlainText == false)
      Text->PlainText = true;
    else
      Text->PlainText = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SaveDialogTypeChange(TObject *Sender)
{
     switch(SaveDialog->FilterIndex)
      {
        case 1:
          ChangeFileExt(SaveDialog->FileName,".txt") ;
         break;

        case 2:
          ChangeFileExt(SaveDialog->FileName,".rtf");
         break;
      }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::I_D_DateClick(TObject *Sender)
{
     switch(MainForm->date)
       {
          case 0:
            Text->SelText = FormatDateTime("/mm/dd aaaa",Now());
           break;

          case 1:
            Text->SelText = FormatDateTime("/mm/dd aaa",Now());
           break;
       }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::I_D_TimeClick(TObject *Sender)
{
     switch(date)
       {
          case 0:
            Text->SelText = FormatDateTime("hh:nn:ss",Now());
           break;

          case 1:
            Text->SelText = FormatDateTime("AM/PM hh:nn:ss",Now());
           break;
       }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FB_ThistextClick(TObject *Sender)
{
     Text->Lines->LoadFromFile(MainForm->backupfilename);
     MainForm->filename = ChangeFileExt(backupfilename,"txt");
     Text->Modified = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::S_NextClick(TObject *Sender)
{
     int position;
     String tmp;

     if(foption.Contains(frDown))
      {
        int start = Text->SelStart;
        if(Text->SelLength != 0)
          ++ start;
        position = Text->FindText(ftext,start,Text->Text.Length() - Text->SelStart - 1,options);
      }
     else if(Text->SelStart > 0 )
      {
        int search = -1;
        do
         {
           position = search;
           search = Text->FindText(ftext,search + 1,Text->SelStart - search - 1,options);
         }
        while(search >= 0);
      }
     else
      position = -1;

     if(position >= 0)
      {
        Text->SelStart = position;
        Text->SelLength = ftext.Length();
      }
     else
      {
        tmp = "「" + ftext +"」が見つかりませんでした。";
        Application->MessageBox(tmp.c_str(),"検索",MB_OK);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::S_LinejumpClick(TObject *Sender)
{
     TRect Rect = Application->MainForm->BoundsRect;
     TLinejumpForm *ljump = new TLinejumpForm(Application);
     ljump->L_LineupDown->Position = (SHORT)SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0)+1;
     ljump->L_LineupDown->Max = (SHORT)SendMessage(Text->Handle,EM_GETLINECOUNT,0,0);
     ljump->Left = ((Rect.Right - Rect.Left) - ljump->Width)/2 + Rect.Left;
     ljump->Top = ((Rect.Bottom - Rect.Top) - ljump->Height)/2 + Rect.Top;
     ljump->ShowModal();
     delete ljump;
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::Tool_JumpContClick(TObject *Sender,
      TUDBtnType Button)
{
     int lineNo;
     lineNo = SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0);


     if(lineNo + 1 < Tool_LineJump->Text.ToInt())
      {
        while( lineNo + 1 != Tool_LineJump->Text.ToInt())
         {
           lineNo++;
         }
      }
     else if(lineNo +1 > Tool_LineJump->Text.ToInt())
      {
        while(lineNo + 1 != Tool_LineJump->Text.ToInt())
         {
           lineNo--;
         }
      }
     Text->SelStart = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
     Text->SelLength = MainForm->Text->Lines->Strings[lineNo].Length();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_UnderlineBtnClick(TObject *Sender)
{
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);


     if(T_UnderlineBtn->Down == true)
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin << fsUnderline;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
      }
     else
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin >> fsUnderline;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
        T_UnderlineBtn->Down = false;
      }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_BraindBtnClick(TObject *Sender)
{
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);


     if(T_BraindBtn->Down == true)
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin << fsStrikeOut;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
      }
     else
      {
        do
          {
             p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
             Text->SelStart = p;
             Text->SelLength = StayleEdit->Text.Length();
             optin = Text->SelAttributes->Style;
             optin >> fsStrikeOut;
             Text->SelAttributes->Style = optin;
          }while( p != -1 );
        T_BraindBtn->Down = false;
      }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_FontBtnClick(TObject *Sender)
{

     TFont *oldfont = new TFont;
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);

     if(T_FontBtn->Down == true)
      {
        if(FontDialog1->Execute())
         {
           do
            {

               p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
               Text->SelStart = p;
               Text->SelLength = StayleEdit->Text.Length();
               oldfont->Assign(Text->SelAttributes);
               optin = Text->SelAttributes->Style;
               Text->SelAttributes->Assign(FontDialog1->Font);
               Text->SelAttributes->Style = optin;
            }while( p != -1 );
         }
        else
          T_FontBtn->Down = false;
       }
      else
       {
         do
            {
               p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
               Text->SelStart = p;
               Text->SelLength = StayleEdit->Text.Length();
               optin = Text->SelAttributes->Style;
               Text->SelAttributes->Assign(oldfont);
               Text->SelAttributes->Style = optin;
            }while( p != -1 );
       }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Tool_FindKeyPress(TObject *Sender, char &Key)
{
     if(Key == VK_RETURN)
      {


        ftext = Tool_Find->Text;
        int position;

        if(foption.Contains(frDown))
         {
           int start = Text->SelStart;
           if(Text->SelLength != 0)
           ++ start;
           position = Text->FindText(ftext,start,Text->Text.Length() - Text->SelStart - 1,options);
         }
        else if(Text->SelStart > 0 )
         {
           int search = -1;
           do
            {
              position = search;
              search = Text->FindText(ftext,search + 1,Text->SelStart - search - 1,options);
            }
           while(search >= 0);
        }
       else
         position = -1;

       if(position >= 0)
        {
          Text->SelStart = position;
          Text->SelLength = ftext.Length();
          if(SendMessage(Tool_Find->Handle,CB_FINDSTRING,-1,(LPARAM)Tool_Find->Text.c_str()) == CB_ERR)
            Tool_Find->Items->Add(Tool_Find->Text);
        }
     }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Tool_LineJumpKeyPress(TObject *Sender,
      char &Key)
{
     if( !(Key >= '0' && Key <= '9') && !(Key ==  VK_RETURN ) && !(Key == '\b') )
       Key = 0;
  /*   if(Key == VK_RETURN)
      {
         int lineNo;
         lineNo = SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0);


         if(lineNo + 1 < Tool_LineJump->Text.ToInt())
          {
            while( lineNo + 1 != Tool_LineJump->Text.ToInt())
             {
               lineNo++;
             }
          }
         else if(lineNo +1 > Tool_LineJump->Text.ToInt())
          {
            while(lineNo + 1 != Tool_LineJump->Text.ToInt())
             {
               lineNo--;
             }
          }
         Text->SelStart = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
         Text->SelLength = MainForm->Text->Lines->Strings[lineNo].Length();
     } */
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
     if(Text->Modified == true)
      {
         switch(Application->MessageBox("ﾌｧｲﾙが変更されています。\n変更を保存しますか？","Coution!!",MB_YESNOCANCEL))
           {
             case IDYES:
               F_SaveClick(Sender);
               break;
             case IDNO:
               CanClose = true;
               break;
             case IDCANCEL:
               CanClose = false;
           }
      }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::F_ExitClick(TObject *Sender)
{
     if(filename != "")
       CloseText();
     Close();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Tool_FindChange(TObject *Sender)
{
     String value = Tool_Find->Text;

     if(T_Findlastkey == '\b' || T_Findlastkey == VK_DELETE)
      {
        T_Findlastkey = 0;
        return;
      }

     T_Findlastkey = 0;

     if(Tool_Find->SelStart != value.Length())
       return;

     int index = SendMessage(Tool_Find->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str());

     if(index >= 0)
      {
        Tool_Find->ItemIndex = index;
        String newtext = Tool_Find->Text;
        SendMessage(Tool_Find->Handle,(CB_SETEDITSEL),0,MAKELPARAM(value.Length(),-1));
      }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Tool_FindKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
      T_Findlastkey = Key;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::StayleEditChange(TObject *Sender)
{
     String value = StayleEdit->Text;

     if(T_Findlastkey == '\b' || T_Findlastkey == VK_DELETE)
      {
        T_Findlastkey = 0;
        return;
      }

     T_Findlastkey = 0;

     if(StayleEdit->SelStart != value.Length())
       return;

     int index = SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str());

     if(index >= 0)
      {
        StayleEdit->ItemIndex = index;
        String newtext = StayleEdit->Text;
        SendMessage(StayleEdit->Handle,CB_SETEDITSEL,0,MAKELPARAM(value.Length(),-1));
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::StayleEditKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
     T_Findlastkey = Key;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Tool_LineJumpChange(TObject *Sender)
{
     if(Tool_LineJump->Text < "1")
      {
        Tool_LineJump->Text = "0";

        //Tool_LineJump->SelLength = Tool_LineJump->Text.Length();
      }

     if(Tool_LineJump->Text.ToInt() > SendMessage(Text->Handle,EM_GETLINECOUNT,0,0))
       Tool_LineJump->Text =SendMessage(Text->Handle,EM_GETLINECOUNT,0,0);
     //SetTextAlign(Tool_LineJump->Handle,TA_RIGHT);

     int lineNo;
     lineNo = SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0);


     if(lineNo + 1 < Tool_LineJump->Text.ToInt())
       {
          while( lineNo + 1 != Tool_LineJump->Text.ToInt())
            {
              lineNo++;
            }
       }
     else if(lineNo +1 > Tool_LineJump->Text.ToInt())
       {
          while(lineNo + 1 != Tool_LineJump->Text.ToInt())
            {
                lineNo--;
            }
       }
     Text->SelStart = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
     Text->SelLength = MainForm->Text->Lines->Strings[lineNo].Length();

     Tool_LineJump->SelLength = Tool_LineJump->Text.Length();

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SizeBoxChange(TObject *Sender)
{
     Text->SelAttributes->Size = SizeBox->Text.ToInt();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::CloseText()
{
     String rpath = ExtractFileDir(Application->ExeName);
     rpath = rpath + "\\fileinfo\\";
     if (!DirectoryExists(rpath))
      {
        if (!CreateDir(rpath))
           ShowMessage("Cannot create .\\fileinfo directory.");
      }
     rpath = rpath + ExtractFileName(MainForm->filename);
     rpath = ChangeFileExt(rpath,".ini");
     TIniFile *setini = new TIniFile(rpath);

     setini->WriteString("Header","File Name",ExtractFileName(ChangeFileExt(MainForm->filename,"")));
     setini->WriteString("Header","File Path",ExtractFilePath(MainForm->filename));

     if(ExtractFileExt(MainForm->filename) == ".rtf")
       setini->WriteString("Header","File Type","Rich Text Format File");
     else if(ExtractFileExt(MainForm->filename) == ".txt")
       setini->WriteString("Header","File Type","Plane Text File");
     else if(ExtractFileExt(MainForm->filename) == ".doc")
       setini->WriteString("Header","File Type","Microsoft Word Docment File");
     else
       setini->WriteString("Header","File Type","Unkown Format File");

     setini->WriteString("Header","Backup File Name",MainForm->backupfilename);

     setini->WriteInteger("Header","MaxLength",Text->Lines->Count);

     if(endbookmark)
         setini->WriteInteger( "Marked","Last Marke",Text->SelStart);
     else
         setini->WriteInteger("Marked","Last Marke",0);

     setini->WriteString("List","Find",Tool_Find->Items->Text);
     setini->WriteString("List","Read Assistant",StayleEdit->Items->Text);

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenText()
{
     String rpath = ExtractFileDir(Application->ExeName);
     rpath = rpath + "\\fileinfo\\";
     if (!DirectoryExists(rpath))
      {
        if (!CreateDir(rpath))
           ShowMessage("Cannot create .\\fileinfo directory.");
      }
     rpath = rpath + ExtractFileName(MainForm->filename);
     rpath = ChangeFileExt(rpath,".ini");
     TIniFile *setini = new TIniFile(rpath);

     //setini->WriteString("Header","File Name",ChangeFileExt(MainForm->filename,""));
     //setini->WriteString("Header","File Path",ExtractFilePath(MainForm->filename));

     /*if(ExtractFileExt(MainForm->filename) == ".rtf")
       setini->WriteString("Header","File Type","Rich Text Format File");
     else if(ExtractFileExt(MainForm->filename) == ".txt")
       setini->WriteString("Header","File Type","Plane Text File");
     else if(ExtractFileExt(MainForm->filename) == ".doc")
       setini->WriteString("Header","File Type","Microsoft Word Docment File");
     else
       setini->WriteString("Header","File Type","Unkown Format File");

     setini->WriteInteger("Header","MaxLength",Text->Lines->Count);

 */
     if(endbookmark)
         Text->SelStart = setini->ReadInteger("Marked","Last Marke",0);
     else
         Text->SelStart = 0;

     createbackupchk = setini->ReadBool("Save","CreateBackup",true);
     MainForm->backupfilename = setini->ReadString("Header","Backup File Name","");
     Tool_Find->Items->Text = setini->ReadString("List","Find","");
     StayleEdit->Items->Text = setini->ReadString("List","Read Assistant","");
     int End,Pos,Start;
     if(BookMarkMenu->Count > 3 && MainForm->filename != "")
      {
        End = BookMarkMenu->Count-1;
        Start = BookMarkMenu->IndexOf(N20) + 1;
        for(Pos = Start;Pos <= End ;Pos++)
          BookMarkMenu->Delete(Start);

      }
     bookmark = new TBookMark(9,BookMarkMenu,N20);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::F_SaveTextClick(TObject *Sender)
{
     if(MainForm->filename == "" )
      {
          F_SaveAsClick(Sender);
      }
     else
      {

         if(createbackupchk)
           MainForm->backupfilename = MainForm->CreateBackupFile(ChangeFileExt(MainForm->filename,"txt"));
         Text->Lines->SaveToFile(ChangeFileExt(MainForm->filename,"txt"));
         Text->Modified = false;
         CloseText();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ReadSetupini()
{
     String rpath = ExtractFileDir(Application->ExeName);
     rpath = rpath + "\\setup.ini";
     TIniFile *setini = new TIniFile(rpath);
     MainForm->date = setini->ReadInteger("DATE","Type",0);
     MainForm->imechk = setini->ReadBool("Startup","IME",false);
     MainForm->createbackupchk = setini->ReadBool("Save","CreateBackup",true);
     MainForm->endbookmark = setini->ReadBool("Shutdown","BookMark",true);
     MainForm->Sign = setini->ReadString("Infomation","Sign","");
     delete setini;
}
//---------------------------------------------------------------------------
String __fastcall TMainForm::CreateBackupFile(String filename)
{
     String bpath = ExtractFileDir(Application->ExeName);
     bpath = bpath + "\\backup\\";
     if (!DirectoryExists(bpath))
      {
        if (!CreateDir(bpath))
           ShowMessage("Cannot create .\\backup directory.");
      }
     bpath = bpath + ExtractFileName(filename);
     bpath = ChangeFileExt(bpath,".wbf");
     Text->Lines->SaveToFile(bpath);
     return bpath;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::T_ColorBtnClick(TObject *Sender)
{
    // TColor *oldColor = new TColor;
     TSearchTypes options;
     Graphics::TFontStyles optin;
     int p=-1,s;
     s = Text->SelStart;
     Screen->Cursor = crHourGlass;
     Text->SelStart = 0;

     String value = StayleEdit->Text;
     if(SendMessage(StayleEdit->Handle,CB_FINDSTRING,-1,(LPARAM)value.c_str()) == CB_ERR)
       StayleEdit->Items->Add(StayleEdit->Text);

     if(T_ColorBtn->Down == true)
      {
        if(ColorDialog1->Execute())
         {
           do
            {

               p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
               Text->SelStart = p;
               Text->SelLength = StayleEdit->Text.Length();
               oldColor = Text->SelAttributes->Color;
               optin = Text->SelAttributes->Style;
               Text->SelAttributes->Color = ColorDialog1->Color;
               Text->SelAttributes->Style = optin;
            }while( p != -1 );
         }
        else
          T_ColorBtn->Down = false;
       }
      else
       {
         do
            {
               p = Text->FindText(StayleEdit->Text,p+1,Text->Lines->Text.Length(),options << stMatchCase);
               Text->SelStart = p;
               Text->SelLength = StayleEdit->Text.Length();
               optin = Text->SelAttributes->Style;
               Text->SelAttributes->Color = oldColor;
               Text->SelAttributes->Style = optin;
            }while( p != -1 );
       }
     Text->Refresh();
     Text->SelStart = s;
     Screen->Cursor = crDefault;

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::B_AddClick(TObject *Sender)
{
     String rpath = ExtractFileDir(Application->ExeName);
     rpath = rpath + "\\fileinfo\\";
     if (!DirectoryExists(rpath))
      {
        if (!CreateDir(rpath))
           ShowMessage("Cannot create .\\fileinfo directory.");
      }
     rpath = rpath + ExtractFileName(MainForm->filename);
     rpath = ChangeFileExt(rpath,".ini");
     TIniFile *setini = new TIniFile(rpath);
     String stmp = Text->Lines->Strings[SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0)];
     if(stmp.Length() >= 30)
      {
        stmp.Insert(".",27);
        stmp.Insert(".",28);
        stmp.Insert(".",29);
        stmp.Delete(30,stmp.Length() + 3);
      }
     i++;
     while(setini->ReadString("BookMark","BookMark[" + IntToStr(i) + "]","") == "" && stmp != "" )
      {
        if(setini->ReadString("BookMark","BookMark[" + IntToStr(i) + "]","") == stmp)
          break;
        setini->WriteString("BookMark","BookMark[" + IntToStr(i) + "]",stmp);
      }
     delete setini;
     bookmark = new TBookMark(9,BookMarkMenu,N20);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UpdateStatusBar()
{
     int line = SendMessage(Text->Handle,EM_LINEFROMCHAR,(WPARAM)Text->SelStart,0);
     int lineindex = SendMessage(Text->Handle,EM_LINEINDEX,line,0);
     StatusBar1->Panels->Items[0]->Text = IntToStr(line+1) +  " : " + IntToStr(Text->SelStart - lineindex + 1);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::WMDropFiles(TWMDropFiles &message)
{
     TObject *Sender;
     UINT filecount = DragQueryFile((HDROP)message.Drop,0xFFFFFFFF,NULL,0);
     UINT i = filecount;
     if(filecount < 2)
      {
        String fn;
        fn.SetLength(MAX_PATH);
        int length = DragQueryFile((HDROP)message.Drop,i-1,fn.c_str(),fn.Length());
        fn.SetLength(length);

        OpenFile(fn,Sender);
      }
     DragFinish((HDROP)message.Drop);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::V_HistoryClipboardClick(TObject *Sender)
{
     TClipboardForm *clipbrd = new TClipboardForm(Application);
     if(clipformchk == false)
      {

        for(int pos = 0;pos < cliphist->Count;pos ++)
        {
          if(SendMessage(clipbrd->ClipboardHistoryList->Handle ,LB_FINDSTRING,-1,(LPARAM)cliphist->Strings[pos].c_str()) == LB_ERR)
           clipbrd->ClipboardHistoryList->Items->Add(cliphist->Strings[pos]);
        }

        int screenx,screeny;
        screenx = GetSystemMetrics(SM_CXSCREEN);
        screeny = GetSystemMetrics(SM_CYSCREEN);

        clipbrd->Top = MainForm->Top;
        clipbrd->Left = MainForm->Width + MainForm->Left;
        if(screenx < clipbrd->Left + clipbrd->Width )
          MainForm->Width = MainForm->Width - ((clipbrd->Width + clipbrd->Left)-screenx);
        clipbrd->Left = MainForm->Width + MainForm->Left;
        clipbrd->Show();
        clipformchk = true;
      }
}
//---------------------------------------------------------------------------









