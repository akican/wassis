//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "FileHistry.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
TFileHistory::TFileHistory(int theMaxHistoryItems,String theHistoryFileName,TMenuItem *theFileMenu,TMenuItem *theTop,void(__closure *theOpenFileFunc)(String file,TObject *Sender))
{
     MaxHistoryItems = theMaxHistoryItems;
     HistoryFileName = theHistoryFileName;
     FileMenu = theFileMenu;
     OpenFileFunc = theOpenFileFunc;
     LastFile = "";
     chk = 0;

     Top = theTop;
 //    Buttom = NULL;
     HistoryList = new TStringList;
     try
      {
         HistoryList->LoadFromFile(HistoryFileName);
         RebuildHistory();

      }
     catch(...)
      {
      }
}
//---------------------------------------------------------------------------
TFileHistory::~TFileHistory(void)
{
     MaintainHistory("");
     try
      {
        HistoryList->SaveToFile(HistoryFileName);
      }
     catch(...)
      {
        MessageBox(NULL,"エラーが起きました\n履歴を保存できませんでした。","えら〜",MB_ICONERROR|MB_OK);
      }
        HistoryList->Free();
}
//---------------------------------------------------------------------------
void TFileHistory::RebuildHistory()
{
      TMenuItem *MenuItem;
      int Pos,HistoryStart,HistoryEnd;
      HistoryStart = FileMenu->IndexOf(Top)+1;
      if(chk != 0)//Buttom != NULL)
       {
         HistoryEnd = FileMenu->Count-1;//FileMenu->IndexOf(Buttom);
         for(Pos = HistoryStart; Pos <= HistoryEnd;Pos++)
           {
             FileMenu->Delete(HistoryStart);
           }
       }
      if(HistoryList->Count > MaxHistoryItems)
        for(Pos = MaxHistoryItems;Pos < HistoryList->Count;Pos++)
          {
             HistoryList->Delete(MaxHistoryItems);
          }
      if(HistoryList->Count > 0)
       {
         for(Pos = 0;Pos <= HistoryList->Count-2;Pos++)
           {
             MenuItem = new TMenuItem(FileMenu);
             MenuItem->Caption = "&" + IntToStr(Pos+1) + " " + HistoryList->Strings[Pos];
             MenuItem->OnClick = HistoryItemClick;
             FileMenu->Insert(HistoryStart + Pos,MenuItem);
           }
             MenuItem = new TMenuItem(FileMenu);
             MenuItem->Caption = "&" + IntToStr(Pos+1) + " " + HistoryList->Strings[Pos];
             MenuItem->OnClick = HistoryItemClick;
             FileMenu->Insert(HistoryStart + Pos,MenuItem);


       chk = 1;

    //     Buttom = new TMenuItem(FileMenu);
    //     Buttom->Caption = "-";
    //     FileMenu->Insert(HistoryStart + HistoryList->Count,Buttom);
        // FileMenu->Delete(HistoryStart + HistoryList->Count);
       }
 }
 //--------------------------------------------------------------------------
 void __fastcall TFileHistory::HistoryItemClick(TObject *Sender)
 {
      int Pos;
      String NewFile;
      Pos = FileMenu->IndexOf((TMenuItem *)Sender) - FileMenu->IndexOf(Top)-1;
      NewFile = HistoryList->Strings[Pos];
      OpenFileFunc(NewFile,Sender);
 }
 //--------------------------------------------------------------------------
 void TFileHistory::MaintainHistory(String NewFile)
 {
      int Pos;
      if(LastFile != "")
       {
         HistoryList->Insert(0,LastFile);
       }
      if( NewFile != "")
       {
         Pos = HistoryList->IndexOf(NewFile);
         if(Pos != -1)
           HistoryList->Delete(Pos);
       }

      LastFile = NewFile;
      RebuildHistory();
 }


