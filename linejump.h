//---------------------------------------------------------------------------
#ifndef linejumpH
#define linejumpH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TLinejumpForm : public TForm
{
__published:	// IDE 管理のコンポーネント
        TBitBtn *BitBtn2;
        TUpDown *L_LineupDown;
        TEdit *L_LineText;
        TLabel *Label1;
        void __fastcall L_LineTextChange(TObject *Sender);
        void __fastcall BitBtn2Click(TObject *Sender);
private:	// ユーザー宣言
public:		// ユーザー宣言
        __fastcall TLinejumpForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLinejumpForm *LinejumpForm;
//---------------------------------------------------------------------------
#endif
