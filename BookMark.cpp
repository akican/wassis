//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "BookMark.h"
#include "main.h"
#include <FileCtrl.hpp>
//---------------------------------------------------------------------------
TBookMark::TBookMark(int theMaxBookMarkItems,TMenuItem *theBookMarkMenu,TMenuItem* theTop)
{
    MaxMarkItems = theMaxBookMarkItems;
   // BookMarkName = theBookMarkName;
    BookMarkMenu = theBookMarkMenu;
   //ReadFileName = theReadFileName;
    Top = theTop;
    chk = 0;
    BookMarkList = new TStringList;
    try
     {
       if(MainForm->filename != "")
        {
          ReadBookMarkList();
          RebuildMark();
        }
     }
    catch(...)
     {
     }
}
//---------------------------------------------------------------------------
TBookMark::~TBookMark()
{
    BookMarkList->Free();
}
//---------------------------------------------------------------------------
void __fastcall TBookMark::ReadBookMarkList()
{
     int i = 0;
     String rpath = ExtractFileDir(Application->ExeName);
     rpath = rpath + "\\fileinfo\\";
     if (!DirectoryExists(rpath))
      {
        if (!CreateDir(rpath))
           ShowMessage("Cannot create .\\fileinfo directory.");
      }
     rpath = rpath + ExtractFileName(MainForm->filename);
     rpath = ChangeFileExt(rpath,".ini");
     TIniFile *setini = new TIniFile(rpath);
     while(setini->ReadString("BookMark","BookMark[" + IntToStr(i) + "]","") != "")
       {
         BookMarkList->Add(setini->ReadString("BookMark","BookMark[" + IntToStr(i) + "]",""));
         i++;
       }
}
//---------------------------------------------------------------------------
void TBookMark::RebuildMark()
{
     TMenuItem *MenuItem;
     int Pos,MarkStart,MarkEnd;

     MarkStart = BookMarkMenu->IndexOf(Top) + 1;
     if(chk != 0)
      {
         MarkEnd = BookMarkMenu->Count - 1;
         for(Pos = MarkStart;Pos <= MarkEnd;Pos ++)
          BookMarkMenu->Delete(MarkStart);
      }

     if(BookMarkList->Count > MaxMarkItems)
       for(Pos = MaxMarkItems;Pos < BookMarkList->Count;Pos++)
         BookMarkList->Delete(MaxMarkItems);

     if(BookMarkList->Count > 0)
      {
        for(Pos = 0;Pos < BookMarkList->Count;Pos++)
          {
            MenuItem = new TMenuItem(BookMarkMenu);
            MenuItem->Caption = "&" + IntToStr(Pos + 1) + " " + BookMarkList->Strings[Pos];
            MenuItem->OnClick = BookMarkItemClick;
            BookMarkMenu->Insert(MarkStart + Pos,MenuItem);
          }
        chk = 1;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBookMark::BookMarkItemClick(TObject *Sender)
{
     int Pos;
     String NewLine;
     Pos = BookMarkMenu->IndexOf((TMenuItem *)Sender) - BookMarkMenu->IndexOf(Top) - 1;
     NewLine = BookMarkList->Strings[Pos];
     if(NewLine.Length() <= 30)
      NewLine.Delete(27,30);

     int lineNo;
    // TSearchTypes opt;
    // opt << stMatchCase;
    // lineNo = MainForm->Text->FindText(NewLine,0,MainForm->Text->Text.Length(),opt);
     lineNo = MainForm->Text->Text.AnsiPos(NewLine);
     lineNo = SendMessage(MainForm->Text->Handle,EM_LINEFROMCHAR,(WPARAM)lineNo,0);
     MainForm->Text->SelStart = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
     MainForm->Text->SelLength = MainForm->Text->Lines->Strings[lineNo].Length();

}
//---------------------------------------------------------------------------
#pragma package(smart_init)
