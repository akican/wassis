//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "splashfo.h"
#include "main.h"
USERES("Project1.res");
USEFORM("main.cpp", MainForm);
USEFORM("Setup.cpp", SetupForm);
USEUNIT("FileHistry.cpp");
USEFORM("splashfo.cpp", Splash);
USEFORM("linejump.cpp", LinejumpForm);
USEUNIT("BookMark.cpp");
USEFORM("ClipbordHitory.cpp", ClipboardForm);
USEFORM("virsion.cpp", Form1);
USEUNIT("k_IME.cpp");
USELIB("C:\Program Files\Borland\CBuilder5\Lib\PSDK\imm32.lib");
USEUNIT("richedit.cpp");
USEUNIT("k_ClipBoard.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Splash = new TSplash(Application);
                 Splash->Show();
                 Splash->Update();
                 Application->Initialize();
                 Application->Title = "Word Assistant";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 if(ParamStr(1) != "")
                  {
                    TObject *Sender;
                    MainForm->OpenFile(ParamStr(1),Sender);
                  }
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
