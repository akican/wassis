//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "linejump.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLinejumpForm *LinejumpForm;
//---------------------------------------------------------------------------
__fastcall TLinejumpForm::TLinejumpForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLinejumpForm::L_LineTextChange(TObject *Sender)
{
     int lineNo;//len,index;
     lineNo = SendMessage(MainForm->Text->Handle,EM_LINEFROMCHAR,(WPARAM)MainForm->Text->SelStart,0);


     if(lineNo + 1 < L_LineText->Text.ToInt())
      {
        while( lineNo + 1 != L_LineText->Text.ToInt())
         {
           lineNo++;
         }
      }
     else if(lineNo +1 > L_LineText->Text.ToInt())
      {
        while(lineNo + 1 != L_LineText->Text.ToInt())
         {
           lineNo--;
         }
      }
     MainForm->Text->SelStart = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
     MainForm->Text->SelLength = MainForm->Text->Lines->Strings[lineNo].Length();
     //index = SendMessage(MainForm->Text->Handle,EM_LINEINDEX,lineNo,0);
     //len = SendMessage(MainForm->Text->Handle,EM_LINELENGTH,lineNo,0);
     //SendMessage(MainForm->Text->Handle,EM_SETSEL,index,len);
}
//---------------------------------------------------------------------------
void __fastcall TLinejumpForm::BitBtn2Click(TObject *Sender)
{
     Close();
}
//---------------------------------------------------------------------------
