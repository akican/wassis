//---------------------------------------------------------------------------
#ifndef ClipbordHitoryH
#define ClipbordHitoryH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TClipboardForm : public TForm
{
__published:	// IDE 管理のコンポーネント
        TListBox *ClipboardHistoryList;
        TBitBtn *BitBtn1;
        TBitBtn *BitBtn2;
        TBitBtn *BitBtn3;
        TLabel *Label2;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormDeactivate(TObject *Sender);
        void __fastcall ClipboardHistoryListDblClick(TObject *Sender);
private:	// ユーザー宣言
public:		// ユーザー宣言
        __fastcall TClipboardForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipboardForm *ClipboardForm;
//---------------------------------------------------------------------------
#endif
