//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "splashfo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSplash *Splash;
//---------------------------------------------------------------------------
__fastcall TSplash::TSplash(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSplash::Timer2Timer(TObject *Sender)
{
     Close();
}
//---------------------------------------------------------------------------
void __fastcall TSplash::CreateParams(TCreateParams &Params)
{
     TForm::CreateParams(Params);
     Params.Style &= ~WS_CAPTION;
}
//---------------------------------------------------------------------------
void __fastcall TSplash::FormClose(TObject *Sender, TCloseAction &Action)
{
     Action = caFree;        
}
//---------------------------------------------------------------------------

void __fastcall TSplash::Image1Click(TObject *Sender)
{
     Close();
}
//---------------------------------------------------------------------------

