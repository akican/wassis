//---------------------------------------------------------------------------
#include <windows.hpp>
#include <windowsx.h>
#include <imm.h>
#ifndef k_IMEH
#define k_IMEH

#define KIME_ROMA 0
#define KIME_KANA 1

#define KIME_HIRAGANA 0
#define KIME_WIDEKATAKANA 1
#define KIME_KATAKANA 2
#define KIME_WIDEALPHABET 3
#define KIME_ALPHABET 4
#define KIME_DIRECT 5

#define KIME_WORDREGIST IME_CONFIG_REGISTERWORD
#define KIME_OPENDICTIONARY IME_CONFIG_SELECTDICTIONARY
#define KIME_CONFIG IME_CONFIG_GENERAL

#define KIME_BVTEATTRIBUTE GCS_COMPATTR
#define KIME_BYTECLAUSE GCS_COMPCLAUSE
#define KIME_INTATTRIBUTE GCS_COMPREADATTR
#define KIME_CHARCLAUSE GCS_COMPREADCLAUSE
#define KIME_KANASTRING GCS_COMPREADSTR
#define KIME_STRING GCS_COMPSTR
#define KIME_POSITION GCS_CORSORPOS
#define KIME_LASTPOSITION GCS_DELTASTART
#define KIME_LASTBYTECLAUSE GCS_RESULTCLAUSE
#define KIME_LASTCHARCLAUSE GCS_RESULTREADCLAUSE
#define KIME_LASTKANASTRING GCS_RESULTREADSTR

#define KIME_ATOK12LIMIT 200
//---------------------------------------------------------------------------
class K_IME
{
        HWND hwnd;
        HIMC himc;
        HKL  hkl;
        BOOL ime_opened;
        DWORD dwConv,dwSentence;
public:
        K_IME(HWND hhandle,DWORD Keylayout=0L);
        K_IME();
        ~K_IME();
        void Init(HWND hhandle,DWORD Keylayout=0L); // 初期化
        void Switch(BOOL mode); // On/Off切り替え
        BOOL Opened();          // On/Offの取得
        int set_InputType(int mode); // 入力言語の切り替え
        int get_InputType();         // 入力言語の取得
        int set_CharType(int mode);  // 入力文字種の切り替え
        int get_CharType();          // 入力文字種の取得
        void open_Dialog(DWORD config); // 設定画面の呼び出し
        void get_String(char *string,int length=KIME_ATOK12LIMIT); //入力中の文字列の取得
};

#endif
 