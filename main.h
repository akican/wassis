//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include "CSPIN.h"
#include <Dialogs.hpp>
#include "FileHistry.h"
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include "BookMark.h"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE 管理のコンポーネント
        TMainMenu *MainMenu1;
        TMenuItem *FileMenu;
        TMenuItem *F_New;
        TMenuItem *N1;
        TMenuItem *F_Open;
        TMenuItem *F_Save;
        TMenuItem *F_SaveAs;
        TMenuItem *N2;
        TMenuItem *F_PageSetup;
        TMenuItem *F_Print;
        TMenuItem *N3;
        TMenuItem *F_Exit;
        TMenuItem *EditMenu;
        TMenuItem *E_Undo;
        TMenuItem *E_Redo;
        TMenuItem *N4;
        TMenuItem *E_Cut;
        TMenuItem *E_Copy;
        TMenuItem *E_Paste;
        TMenuItem *N5;
        TMenuItem *E_WordWorp;
        TMenuItem *FormMenu;
        TMenuItem *F_Font;
        TMenuItem *InsertMenu;
        TMenuItem *I_Date;
        TMenuItem *I_D_All;
        TMenuItem *I_D_Date;
        TMenuItem *I_D_Time;
        TMenuItem *I_D_Year;
        TMenuItem *N6;
        TMenuItem *I_Sign;
        TMenuItem *FindMenu;
        TMenuItem *S_Search;
        TMenuItem *S_Next;
        TMenuItem *Help;
        TMenuItem *H_Topic;
        TMenuItem *N7;
        TMenuItem *H_Version;
        TStatusBar *StatusBar1;
        TToolBar *ToolBar2;
        TComboBox *FontBox;
        TToolButton *ToolButton4;
        TLabel *Label1;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TPrintDialog *PrintDialog1;
        TPrinterSetupDialog *PrinterSetupDialog1;
        TFindDialog *FindDialog1;
        TReplaceDialog *ReplaceDialog1;
        TMenuItem *N8;
        TMenuItem *S_Replace;
        TPopupMenu *PopupMenu1;
        TMenuItem *P_Undo;
        TMenuItem *N9;
        TMenuItem *P_Cut;
        TMenuItem *P_Copy;
        TMenuItem *P_Paste;
        TMenuItem *N10;
        TMenuItem *P_Font;
        TMenuItem *A1;
        TMenuItem *N11;
        TMenuItem *E_All;
        TMenuItem *N12;
        TMenuItem *SeupMenu;
        TMenuItem *F_Reload;
        TMenuItem *F_OpenBackup;
        TMenuItem *F_History;
        TMenuItem *S_Linejump;
        TMenuItem *N13;
        TMenuItem *F_Left;
        TMenuItem *F_Center;
        TMenuItem *F_Right;
        TMenuItem *N14;
        TMenuItem *FB_Thistext;
        TMenuItem *N15;
        TMenuItem *FB_Othertext;
        TMenuItem *u1;
        TComboBox *SizeBox;
        TMenuItem *F_Bullet;
        TOpenDialog *OpenBackDialog;
        TFontDialog *FontDialog1;
        TMenuItem *IMEMenu;
        TMenuItem *J_Switch;
        TMenuItem *J_RegistWord;
        TMenuItem *J_ChangeDictionaly;
        TMenuItem *N16;
        TMenuItem *J_Propatiy;
        TMenuItem *N17;
        TMenuItem *J_Mode;
        TMenuItem *JM_Lhira;
        TMenuItem *JW_Lkata;
        TMenuItem *JM_Skana;
        TMenuItem *JM_Lalpha;
        TMenuItem *JM_Salpha;
        TMenuItem *N21;
        TMenuItem *JW_Alpha;
        TTimer *Timer1;
        TToolBar *ToolBar3;
        TLabel *Label2;
        TComboBox *Tool_Find;
        TToolButton *ToolButton7;
        TMenuItem *IM_Lang;
        TMenuItem *IL_Roma;
        TMenuItem *IL_Kata;
        TMenuItem *F_NewText;
        TMenuItem *N18;
        TMenuItem *F_Switch;
        TImageList *ImageList1;
        TMenuItem *F_SaveText;
        TEdit *Tool_LineJump;
        TUpDown *Tool_JumpCont;
        TMenuItem *BookMarkMenu;
        TRichEdit *Text;
        TColorDialog *ColorDialog1;
        TMenuItem *B_Add;
        TPageScroller *PageScroller2;
        TToolBar *ToolBar1;
        TToolButton *ToolButton1;
        TToolButton *T_NewBtn;
        TToolButton *ToolButton2;
        TToolButton *T_OpenBtn;
        TToolButton *F_SaveBtn;
        TToolButton *T_SaveAsBtn;
        TToolButton *ToolButton5;
        TToolButton *T_ReloadBtn;
        TToolButton *ToolButton3;
        TMenuItem *C1;
        TMenuItem *N20;
        TMenuItem *ShowMenu;
        TMenuItem *V_HistoryClipboard;
        TToolButton *T_PrintBtn;
        TToolButton *ToolButton8;
        TToolButton *T_UndoBtn;
        TToolButton *ToolButton10;
        TToolButton *T_RedoBtn;
        TToolButton *T_CutBtn;
        TToolButton *T_CopyBtn;
        TToolButton *T_PasteBtn;
        TPageScroller *PageScroller1;
        TLabel *Label3;
        TComboBox *StayleEdit;
        TToolButton *T_BoldBtn;
        TToolButton *T_ItaricBtn;
        TToolButton *ToolButton13;
        TToolButton *T_UnderlineBtn;
        TToolButton *T_BraindBtn;
        TToolButton *T_ColorBtn;
        TToolButton *T_FontBtn;
        TToolButton *ToolButton6;
        TToolButton *T_LeftAlignBtn;
        TToolButton *T_CenterAlignBtn;
        TToolButton *T_RightAlignBtn;
        TToolButton *ToolButton11;
        TToolButton *T_BulletBtn;
        TPageScroller *PageScroller3;
        TToolButton *ToolButton9;
        void __fastcall F_NewClick(TObject *Sender);
        void __fastcall F_OpenClick(TObject *Sender);
        void __fastcall F_SaveClick(TObject *Sender);
        void __fastcall F_SaveAsClick(TObject *Sender);
        void __fastcall SeupMenuClick(TObject *Sender);
        void __fastcall F_PrintClick(TObject *Sender);
        void __fastcall F_PageSetupClick(TObject *Sender);
        void __fastcall E_UndoClick(TObject *Sender);
        void __fastcall E_CutClick(TObject *Sender);
        void __fastcall E_CopyClick(TObject *Sender);
        void __fastcall E_PasteClick(TObject *Sender);
        void __fastcall E_WordWorpClick(TObject *Sender);
        void __fastcall E_AllClick(TObject *Sender);
        void __fastcall F_ReloadClick(TObject *Sender);
        void __fastcall I_D_YearClick(TObject *Sender);
        void __fastcall S_SearchClick(TObject *Sender);
        void __fastcall S_ReplaceClick(TObject *Sender);
        void __fastcall FindDialog1Find(TObject *Sender);
        void __fastcall ReplaceDialog1Replace(TObject *Sender);
        void __fastcall F_FontClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FB_OthertextClick(TObject *Sender);
        void __fastcall F_BulletClick(TObject *Sender);
        void __fastcall F_LeftClick(TObject *Sender);
        void __fastcall T_BoldBtnClick(TObject *Sender);
        void __fastcall T_ItaricBtnClick(TObject *Sender);
        void __fastcall FontBoxChange(TObject *Sender);
        void __fastcall J_SwitchClick(TObject *Sender);
        void __fastcall JW_AlphaClick(TObject *Sender);
        void __fastcall J_RegistWordClick(TObject *Sender);
        void __fastcall J_ChangeDictionalyClick(TObject *Sender);
        void __fastcall J_PropatiyClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall TextKeyPress(TObject *Sender, char &Key);
        void __fastcall I_D_AllClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall I_SignClick(TObject *Sender);
        void __fastcall IMH_KanaClick(TObject *Sender);
        void __fastcall JW_LkataClick(TObject *Sender);
        void __fastcall IL_RomaClick(TObject *Sender);
        void __fastcall JM_LhiraClick(TObject *Sender);
        void __fastcall IL_KataClick(TObject *Sender);
        void __fastcall JM_SkanaClick(TObject *Sender);
        void __fastcall JM_LalphaClick(TObject *Sender);
        void __fastcall JM_SalphaClick(TObject *Sender);
        void __fastcall F_NewTextClick(TObject *Sender);
        void __fastcall F_SwitchClick(TObject *Sender);
        void __fastcall SaveDialogTypeChange(TObject *Sender);
        void __fastcall I_D_DateClick(TObject *Sender);
        void __fastcall I_D_TimeClick(TObject *Sender);
        void __fastcall FB_ThistextClick(TObject *Sender);
        void __fastcall S_NextClick(TObject *Sender);
        void __fastcall S_LinejumpClick(TObject *Sender);
        void __fastcall Tool_JumpContClick(TObject *Sender,
          TUDBtnType Button);
        void __fastcall T_UnderlineBtnClick(TObject *Sender);
        void __fastcall T_BraindBtnClick(TObject *Sender);
        void __fastcall T_FontBtnClick(TObject *Sender);
        void __fastcall Tool_FindKeyPress(TObject *Sender, char &Key);
        void __fastcall Tool_LineJumpKeyPress(TObject *Sender, char &Key);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall F_ExitClick(TObject *Sender);
        void __fastcall Tool_FindChange(TObject *Sender);
        void __fastcall Tool_FindKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall StayleEditChange(TObject *Sender);
        void __fastcall StayleEditKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Tool_LineJumpChange(TObject *Sender);
        void __fastcall SizeBoxChange(TObject *Sender);
        void __fastcall F_SaveTextClick(TObject *Sender);
        void __fastcall T_ColorBtnClick(TObject *Sender);
        void __fastcall B_AddClick(TObject *Sender);
        void __fastcall V_HistoryClipboardClick(TObject *Sender);
private:	// ユーザー宣言
        TFileHistory *FileHistory;
        TBookMark *bookmark;
        void UpdateText();
        void __fastcall UpdateStatusBar();
        void __fastcall OnIdle(TObject *Sender,bool &Done);
        void __fastcall CloseText();
        void __fastcall OpenText();
        void __fastcall ReadSetupini();
        String __fastcall CreateBackupFile(String filename);
        WORD T_Findlastkey;
        virtual void __fastcall WMDropFiles(TWMDropFiles &message);
public:		// ユーザー宣言
        __fastcall TMainForm(TComponent* Owner);
        String filename,backupfilename,ftext,Sign;//Year,Date,Time,Minit,Dayweek,Seconds,YMDTWHMS;
        TSearchTypes options;
        TFindOptions foption;
        TFont *oldfont;
        TColor oldColor;
        TStringList *cliphist;
        int date;
        char imename[100];
        int i;
        int TabNo;
        bool roma;
        bool imechk,endbookmark,createbackupchk;
        bool clipformchk;
        void OpenFile(String file,TObject *Sender);
        BEGIN_MESSAGE_MAP
           VCL_MESSAGE_HANDLER(WM_DROPFILES,TWMDropFiles,WMDropFiles)
        END_MESSAGE_MAP(TForm);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
 