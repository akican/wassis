//---------------------------------------------------------------------------
#ifndef splashfoH
#define splashfoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TSplash : public TForm
{
__published:	// IDE 管理のコンポーネント
        TImage *Image1;
        TTimer *Timer2;
        void __fastcall Timer2Timer(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Image1Click(TObject *Sender);
private:	// ユーザー宣言
        void __fastcall CreateParams(TCreateParams &Params);
public:		// ユーザー宣言
        __fastcall TSplash(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSplash *Splash;
//---------------------------------------------------------------------------
#endif
