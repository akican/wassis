//---------------------------------------------------------------------------
#ifndef FileHistryH
#define FileHistryH
//---------------------------------------------------------------------------
class TFileHistory
{
      private:
         TStringList *HistoryList;
         TMenuItem *Top;//,*Buttom;
         int chk;
         String LastFile;
         TMenuItem *FileMenu;
         int MaxHistoryItems;
         String HistoryFileName;
         void (__closure *OpenFileFunc) (String file,TObject *Sender);
         void RebuildHistory();
         void __fastcall HistoryItemClick(TObject *Sender);

      public:
         TFileHistory(int theMaxHistoryItems,String theHistoryFileName,TMenuItem *theFileMenu,TMenuItem* theTop,void(__closure *theOpenFileFunc)(String file,TObject *Sender));
         ~TFileHistory(void);
         void MaintainHistory(String OldFile);
};


#endif
