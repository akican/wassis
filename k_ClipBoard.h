//---------------------------------------------------------------------------
#include <windows.hpp>
#include <windowsx.h>
#ifndef k_ClipBoardH
#define k_ClipBoardH
//---------------------------------------------------------------------------
class K_ClipBoard
{
        HWND hwnd;
        bool opened;

     public:

        K_ClipBoard(HWND hhandle):
        K_ClipBoard();
        bool Opened();
        bool Open();
        bool Close();
};

#endif
 